package com.okan.springredisexample.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisLongService {

    private final RedisTemplate<String, Long> redisTemplate;

    public RedisLongService(@Qualifier("longRedisTemplate") RedisTemplate<String, Long> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public  Long getLongValue(String key) {
        redisTemplate.opsForValue().set(key, Long.valueOf(key));
        Long value = redisTemplate.opsForValue().get(key);
        return value;
    }
}
