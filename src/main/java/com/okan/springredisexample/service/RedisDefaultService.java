package com.okan.springredisexample.service;

import com.okan.springredisexample.model.User;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class RedisDefaultService {

    private final RedisTemplate redisTemplate;

    public RedisDefaultService(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    //@PostConstruct
    public void initialize() {
        redisTemplate.delete("*");
        User user = new User(3L, "okan", "yildirim");
        redisTemplate.opsForValue().set(user.getId().toString(), user);

        User value = (User) redisTemplate.opsForValue().get("3");
    }

    public String getStringValue(String key) {
        redisTemplate.opsForValue().set(key, key);
        String value = (String) redisTemplate.opsForValue().get(key);
        return value;
    }

    public User getObject( User user) {
        redisTemplate.opsForValue().set(user.getId().toString(), user);
        User value = (User) redisTemplate.opsForValue().get("3");
        return value;
    }

}
