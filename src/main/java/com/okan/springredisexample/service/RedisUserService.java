package com.okan.springredisexample.service;

import com.okan.springredisexample.model.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service
public class RedisUserService {

    private final RedisTemplate<String, User> redisTemplate;

    public RedisUserService(@Qualifier("userRedisTemplate") RedisTemplate<String, User> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    // @PostConstruct
    public void initialize() {
        redisTemplate.delete("*");
        User user = new User(4L, "okan", "yildirim");
        redisTemplate.opsForValue().set(user.getId().toString(), user);

        User fetchedUser = redisTemplate.opsForValue().get(user.getId().toString());

        Set<User> wedf = redisTemplate.opsForZSet().range("5", 0, 0);

        User next = wedf.iterator().next();

    }

    public User getUser(User user) {
        // User user = new User(4L, "okan", "yildirim");
        redisTemplate.opsForValue().set(user.getId().toString(), user);
        User fetchedUser = redisTemplate.opsForValue().get(user.getId().toString());
        return fetchedUser;
    }


    public User getUserWithTTL(User user, int timeOut, TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(user.getId().toString(), user, timeOut, timeUnit);
        User fetchedUser = redisTemplate.opsForValue().get(user.getId().toString());
        return fetchedUser;
    }

    public List<User> addToZSet(String key, List<User> users) {
        users.forEach(user -> redisTemplate.opsForZSet().add(key, user, users.indexOf(user)));
        Set<User> userSet = redisTemplate.opsForZSet().range(key, 0, users.size());

        if (Objects.isNull(userSet) || userSet.isEmpty()) {
            return new ArrayList<>();
        }
        return new ArrayList<>(userSet);
    }

    public Long increment(String key) {
        return redisTemplate.opsForValue().increment(key);
    }

    public Long increment(String key, int count) {
        return redisTemplate.opsForValue().increment(key, count);
    }

    public boolean setExpireTime(String key, int count) {
        return redisTemplate.expire(key, count, TimeUnit.SECONDS);
    }

    public boolean isExpiredAt(String key, Date date) {
        return redisTemplate.expireAt(key, date);
    }

    public boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }
}
