package com.okan.springredisexample;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.GenericContainer;

@ContextConfiguration(initializers = AbstractRedisContainer.Initializer.class)
public class AbstractRedisContainer {

    protected static GenericContainer redisContainer;

    static {
        redisContainer = new GenericContainer("redis:5.0.7-alpine")
                .withExposedPorts(6379, 6379);
        redisContainer.start();
    }

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {

            TestPropertyValues.of(
                    "spring.redis.host=" + redisContainer.getContainerIpAddress(),
                    "spring.redis.port=" + redisContainer.getMappedPort(6379)
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
