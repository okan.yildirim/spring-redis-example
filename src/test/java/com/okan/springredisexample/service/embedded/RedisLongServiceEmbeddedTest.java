package com.okan.springredisexample.service.embedded;

import com.okan.springredisexample.TestEmbeddedRedis;
import com.okan.springredisexample.config.RedisConfig;
import com.okan.springredisexample.service.RedisLongService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Set;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@SpringBootTest(classes = {RedisLongService.class,
        TestEmbeddedRedis.class, RedisConfig.class})
@ImportAutoConfiguration(RedisAutoConfiguration.class)
@Disabled
class RedisLongServiceEmbeddedTest {

    @Autowired
    RedisLongService redisLongService;

    @Autowired
    RedisTemplate<String, Long> redisTemplate;

    @AfterEach
    void tearDown() {
        Set keys = redisTemplate.keys("*");
        keys.stream().forEach( key -> redisTemplate.delete((String) key));
    }

    @Test
    void it_should_set_long_value_and_get_it() {
        //given
        String key = "10";
        //when
        Long value = redisLongService.getLongValue(key);
        //then
        assertThat(value).isEqualTo(Long.valueOf("10"));
    }
}