package com.okan.springredisexample.service.container;

import com.okan.springredisexample.AbstractRedisContainer;
import com.okan.springredisexample.config.RedisConfig;
import com.okan.springredisexample.model.User;
import com.okan.springredisexample.service.RedisUserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static java.util.stream.Collectors.toMap;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest(classes = {RedisUserService.class, RedisConfig.class})
@ImportAutoConfiguration(RedisAutoConfiguration.class)
class RedisUserServiceTest extends AbstractRedisContainer {

    @Autowired
    RedisUserService redisUserService;

    @Autowired
    RedisTemplate<String, User> redisTemplate;

    @AfterEach
    void tearDown() {
        Set keys = redisTemplate.keys("*");
        keys.stream().forEach( key -> redisTemplate.delete((String) key));
    }

    @Test
    void it_should_get_user() {
        //given
        User user = new User(3L, "okan", "yildirim");
        //when
        User value = redisUserService.getUser(user);
        //then
        assertThat(value).isEqualToComparingFieldByField(user);
    }

    @Test
    void it_should_get_user_with_ttl() {
        //given
        User user = new User(3L, "okan", "yildirim");
        //when
        User value = redisUserService.getUserWithTTL(user, 3, TimeUnit.SECONDS);
        //then
        assertThat(value).isEqualToComparingFieldByField(user);
    }

    @Test
    void it_should_not_get_user_when_ttl_up() throws InterruptedException {
        //given
        User user = new User(3L, "okan", "yildirim");
        //when
        User fetched = redisUserService.getUserWithTTL(user, 1, TimeUnit.SECONDS);
        Thread.sleep(1050);
        User value = redisTemplate.opsForValue().get("3");
        //then

        assertThat(fetched).isEqualToComparingFieldByField(user);
        assertThat(value).isNull();
    }

    @Test
    void it_should_get_user_zSet() {
        //given
        User user1 = new User(1L, "okan", "yildirim");
        User user2 = new User(2L, "fatih", "yilmaz");
        User user3 = new User(3L, "hasan", "ari");
        String key = "users";
        //when
        List<User> users = redisUserService.addToZSet(key, List.of(user1, user2, user3));
        //then

        Map<Long, User> userMap = users.stream().collect(toMap(User::getId, user -> user));

        assertThat(userMap.get(user1.getId())).isEqualToComparingFieldByField(user1);
        assertThat(userMap.get(user2.getId())).isEqualToComparingFieldByField(user2);
        assertThat(userMap.get(user3.getId())).isEqualToComparingFieldByField(user3);

        Long size = redisTemplate.opsForZSet().zCard(key);
        assertThat(size).isEqualTo(3);

        // todo other funtion tests
    }

    @Test
    public void it_should_increment() {
        //given
        String key = "key";
         //when
        Long increment = redisUserService.increment(key);
        //then
        assertThat(increment).isEqualTo(1);
    }

    @Test
    public void it_should_increment_as_count() {
        //given
        String key = "key";
        //when
        Long increment = redisUserService.increment(key,5);
        //then
        assertThat(increment).isEqualTo(5);
    }
}