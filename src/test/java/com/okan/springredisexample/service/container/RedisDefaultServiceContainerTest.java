package com.okan.springredisexample.service.container;

import com.okan.springredisexample.AbstractRedisContainer;
import com.okan.springredisexample.config.RedisConfig;
import com.okan.springredisexample.model.User;
import com.okan.springredisexample.service.RedisDefaultService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Set;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@SpringBootTest(classes = {RedisDefaultService.class,
        RedisConfig.class})
@ImportAutoConfiguration(RedisAutoConfiguration.class)
class RedisDefaultServiceContainerTest extends AbstractRedisContainer {

    @Autowired
    RedisDefaultService redisDefaultService;

    @Autowired
    RedisTemplate redisTemplate;

    @AfterEach
    void tearDown() {
        Set keys = redisTemplate.keys("*");
        keys.stream().forEach( key -> redisTemplate.delete(key));
    }

    @Test
    void it_should_set_and_get_for_string() {
        //given
        String key = "key";
        //when
        String value = redisDefaultService.getStringValue(key);
        //then
        assertThat(value).isEqualTo(key);

    }

    @Test
    void it_should_set_and_get_for_user_object() {
        //given
        User user = new User(3L, "okan", "yildirim");
        //when
        User value = redisDefaultService.getObject(user);
        //then
        assertThat(value).isEqualToComparingFieldByField(user);
    }
}